var searchData=
[
  ['imagedata',['ImageData',['../structipb_1_1_image_data.html#abf2814e5779985dad4f78cac080dd09e',1,'ipb::ImageData::ImageData(const cv::Mat &amp;descriptor, const std::string &amp;file_name)'],['../structipb_1_1_image_data.html#aa3c3aa07e60f2560d48d58c0d9c5e587',1,'ipb::ImageData::ImageData(const cv::Mat &amp;descriptor, const std::string &amp;file_name, const Histogram &amp;histogram)'],['../structipb_1_1_image_data.html#ae3572b11b765dbf27b6ed3f917e5a020',1,'ipb::ImageData::ImageData(const std::string &amp;file_name, const Histogram &amp;histogram)']]],
  ['iscomplete',['IsComplete',['../class_writer.html#a07d74d36dd3191b06e0aab678c246157',1,'Writer']]],
  ['iserror',['IsError',['../struct_parse_result.html#adfe0ef5b994e82f8aa9ebf0b30c924b1',1,'ParseResult']]],
  ['isvalid',['IsValid',['../class_generic_schema_validator.html#a8ebda4da3d8b1fc41e57f15dd62e8f19',1,'GenericSchemaValidator']]],
  ['iterativeparsecomplete',['IterativeParseComplete',['../class_generic_reader.html#aa1e9e1eef614fde971550ed2f955151d',1,'GenericReader']]],
  ['iterativeparseinit',['IterativeParseInit',['../class_generic_reader.html#a7de472eda2ad9de13cfd8c1de74f1754',1,'GenericReader']]],
  ['iterativeparsenext',['IterativeParseNext',['../class_generic_reader.html#a257891331e0c259903e7066fb4cebf92',1,'GenericReader']]]
];
