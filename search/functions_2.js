var searchData=
[
  ['calculate_5ftid',['calculate_tid',['../namespaceipb.html#aff7505dc9d306a9806bf6f4dc5f812ca',1,'ipb']]],
  ['calculatedotproduct',['calculateDotProduct',['../namespaceipb.html#a531d203c7be0f295908caf7d94934ee0',1,'ipb']]],
  ['calculateimagenumberwithgivenword',['calculateImageNumberWithGivenWord',['../classipb_1_1_bow_dictionary.html#a6780563d12fd061577c5ec813b43fd58',1,'ipb::BowDictionary']]],
  ['calculatesimilarityindex',['calculateSimilarityIndex',['../namespaceipb.html#a9fb9694a2f92878bcca2acd8c5ed006a',1,'ipb']]],
  ['capacity',['Capacity',['../class_memory_pool_allocator.html#a5672e0833fda2e71ce987911397489ed',1,'MemoryPoolAllocator']]],
  ['cbegin',['cbegin',['../classipb_1_1_histogram.html#a7a668a789a36f1cacd5b10fdb4168a06',1,'ipb::Histogram']]],
  ['cend',['cend',['../classipb_1_1_histogram.html#a72ca9c67dfbce3fab8a5e6c63f902c19',1,'ipb::Histogram']]],
  ['clear',['Clear',['../class_memory_pool_allocator.html#a57bbc80e570db6110901b9a7e36dbda0',1,'MemoryPoolAllocator::Clear()'],['../struct_parse_result.html#a88b6d44f052a19e6436ae6aadc2c40b4',1,'ParseResult::Clear()']]],
  ['cluster',['Cluster',['../classipb_1_1_cluster.html#a829efebcb07c9e08baf24f0cddc87c8a',1,'ipb::Cluster']]],
  ['clusteringalgorithm',['ClusteringAlgorithm',['../classipb_1_1_clustering_algorithm.html#ad12c93e5cd75ccfe2568683a51ba4347',1,'ipb::ClusteringAlgorithm']]],
  ['code',['Code',['../struct_parse_result.html#a2aae3c2f42b31cc2409ee1e03bc4852e',1,'ParseResult']]]
];
