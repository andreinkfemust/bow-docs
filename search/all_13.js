var searchData=
[
  ['validate',['Validate',['../struct_transcoder.html#a8a64aa837f7962894a99f63232472543',1,'Transcoder']]],
  ['value',['Value',['../classcxxopts_1_1_value.html',1,'cxxopts::Value'],['../class_generic_member.html#aad3cfa4f9e8b9018068c8bc865723083',1,'GenericMember::value()'],['../document_8h.html#a071cf97155ba72ac9a1fc4ad7e63d481',1,'Value():&#160;document.h']]],
  ['valuecount',['valueCount',['../struct_writer_1_1_level.html#a4a09e5fda49d0d57b2adc041203f244f',1,'Writer::Level']]],
  ['valueiterator',['ValueIterator',['../class_generic_value.html#aee30721a49688ba0f865f5d581eb6be9',1,'GenericValue']]],
  ['valuetype',['ValueType',['../class_generic_value.html#a43a39bb4fca9b9d3de3da6ac353d25ce',1,'GenericValue::ValueType()'],['../class_generic_document.html#a8936205dc215dda029060d7e835e0549',1,'GenericDocument::ValueType()']]],
  ['vectormattomat',['vectorMatToMat',['../namespaceipb.html#a753cad2dffee8230192de848a835a8e9',1,'ipb']]],
  ['vocabulary',['vocabulary',['../classipb_1_1_bow_dictionary.html#ad249f13b855b74852bccf33afdf13cdc',1,'ipb::BowDictionary']]]
];
