var searchData=
[
  ['hasparseerror',['HasParseError',['../class_generic_document.html#a510a0588db4eb372f5d81bc3646578fb',1,'GenericDocument::HasParseError()'],['../class_generic_reader.html#ac417441794477ea747b63adb6d3653a9',1,'GenericReader::HasParseError()']]],
  ['histogram',['Histogram',['../classipb_1_1_histogram.html#a1163eb0eafaeecc397367c35d2b3b832',1,'ipb::Histogram::Histogram()'],['../classipb_1_1_histogram.html#a63c1ac451e9c7564f0ed28c4879dfeb8',1,'ipb::Histogram::Histogram(const cv::Mat &amp;descriptors, const ipb::BowDictionary &amp;dictionary)'],['../classipb_1_1_histogram.html#a86f6b2a4a8055cc5dc5d7e179dc653a8',1,'ipb::Histogram::Histogram(const std::vector&lt; float &gt; &amp;bins)']]]
];
