var searchData=
[
  ['data',['Data',['../union_generic_value_1_1_data.html',1,'GenericValue&lt; Encoding, Allocator &gt;::Data'],['../classipb_1_1_histogram.html#aaf2cec30b8faab3886241963c78a9221',1,'ipb::Histogram::data()']]],
  ['databasesize',['databaseSize',['../classipb_1_1_bow_dictionary.html#a1803d6fbefa0978a0f7a7135582be5d3',1,'ipb::BowDictionary']]],
  ['decodedstream',['DecodedStream',['../classinternal_1_1_decoded_stream.html',1,'internal']]],
  ['descriptor',['descriptor',['../structipb_1_1_image_data.html#a0a920d73978a74056697ac2a718a13ad',1,'ipb::ImageData']]],
  ['deserialize',['Deserialize',['../namespaceipb_1_1serialization.html#a50d4959fb9d4fdbb3627358f75502257',1,'ipb::serialization']]],
  ['differencetype',['DifferenceType',['../class_generic_member_iterator.html#aaa13c83e6e0d1f5b413d62cacd8f6a2e',1,'GenericMemberIterator']]],
  ['diyfp',['DiyFp',['../structinternal_1_1_diy_fp.html',1,'internal']]],
  ['document',['Document',['../document_8h.html#ac6ea5b168e3fe8c7fa532450fc9391f7',1,'document.h']]],
  ['document_2eh',['document.h',['../document_8h.html',1,'']]],
  ['double',['Double',['../classinternal_1_1_double.html',1,'internal::Double'],['../class_writer.html#a22a43e8a7193105deec6b808736f7a1a',1,'Writer::Double()']]]
];
