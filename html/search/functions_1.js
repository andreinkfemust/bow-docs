var searchData=
[
  ['basicistreamwrapper',['BasicIStreamWrapper',['../class_basic_i_stream_wrapper.html#a3e9a2dd2b6b28243f8f2a911f67cdf56',1,'BasicIStreamWrapper::BasicIStreamWrapper(StreamType &amp;stream)'],['../class_basic_i_stream_wrapper.html#a7a87c6702f1e98256de416ee101a460f',1,'BasicIStreamWrapper::BasicIStreamWrapper(StreamType &amp;stream, char *buffer, size_t bufferSize)']]],
  ['begin',['begin',['../classipb_1_1_histogram.html#ae0bae61fee19e47c4420e7a419b94ecc',1,'ipb::Histogram']]],
  ['binssum',['binsSum',['../classipb_1_1_histogram.html#aa875c2551801403257bef59760943c34',1,'ipb::Histogram']]],
  ['bowdictionary',['BowDictionary',['../classipb_1_1_bow_dictionary.html#a665b4b514dbf8e0d334145a2e2c2cb80',1,'ipb::BowDictionary::BowDictionary(BowDictionary &amp;)=delete'],['../classipb_1_1_bow_dictionary.html#a7247fe91f2c0e87d890ca1f67bf167e5',1,'ipb::BowDictionary::BowDictionary(BowDictionary &amp;&amp;)=delete']]],
  ['build',['build',['../classipb_1_1_bow_dictionary.html#a8795f2858f040ed917b4eae6cbc49410',1,'ipb::BowDictionary']]]
];
