var searchData=
[
  ['rapidjson_5fdisableif_5freturn',['RAPIDJSON_DISABLEIF_RETURN',['../class_generic_value.html#a4a4418a93777942e1fb7ea71f8aaf680',1,'GenericValue::RAPIDJSON_DISABLEIF_RETURN()'],['../class_generic_pointer.html#aaf4d7d852098878d24188d134182d42f',1,'GenericPointer::RAPIDJSON_DISABLEIF_RETURN()']]],
  ['rawassign',['RawAssign',['../class_generic_value.html#abb8ea2dfbe74ff4ee7dac6be31317f81',1,'GenericValue']]],
  ['rawnumber',['RawNumber',['../struct_base_reader_handler.html#a9ed0d83d5e6c8f5e4b32ca3735ff0bb7',1,'BaseReaderHandler']]],
  ['rawvalue',['RawValue',['../class_pretty_writer.html#a440890a72408a150ef46edda6becdc94',1,'PrettyWriter::RawValue()'],['../class_writer.html#ae0d1615104e4e88040b9640e6784008a',1,'Writer::RawValue()']]],
  ['readcentroidsfromcsv',['ReadCentroidsFromCSV',['../namespaceipb.html#ab480d0ef152d0f15b53d7bbd637dbdad',1,'ipb']]],
  ['readfromcsv',['ReadFromCSV',['../classipb_1_1_histogram.html#a2d2ca9d65e3ec8cef5ac4af9d8d99c66',1,'ipb::Histogram::ReadFromCSV()'],['../namespaceipb.html#a02729874e2a6e8f335ea5e9e29910aba',1,'ipb::ReadFromCSV()']]],
  ['readimagedatavector',['ReadImageDataVector',['../namespaceipb.html#a82bfcb0831110460e6436b33309b1d25',1,'ipb']]],
  ['realloc',['Realloc',['../class_memory_pool_allocator.html#aba75280d42184b0ad414243f7f5ac6c7',1,'MemoryPoolAllocator']]],
  ['reset',['Reset',['../class_generic_schema_validator.html#a49efbbe098cb77728be3d48cafed17e4',1,'GenericSchemaValidator::Reset()'],['../class_writer.html#a8b53e8f137f7fcf694f5500711b3f58d',1,'Writer::Reset()']]],
  ['reweighthistograms',['reweightHistograms',['../classipb_1_1_bow_dictionary.html#a23cb25af46527f9b215469b612010c80',1,'ipb::BowDictionary']]]
];
